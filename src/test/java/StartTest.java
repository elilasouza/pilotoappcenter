
import io.appium.java_client.MobileElement;
import org.junit.*;

import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.EnhancedAndroidDriver;
import org.openqa.selenium.By;

import org.junit.Rule;

import static org.junit.Assert.*;


public class StartTest {
    //public static AndroidDriver driver;

    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    private static EnhancedAndroidDriver<MobileElement> driver;

    @Before
    public void setup () throws MalformedURLException {

        //Run test on AppCenter
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("deviceName", "Sony Xperia XZ");
        desiredCapabilities.setCapability("plataformVersion", "6.0.1");
        desiredCapabilities.setCapability("app", "/src/test/resources/Notepad Swift Notes_v1.5_apkpure.com.apk");
        desiredCapabilities.setCapability("appActivity",
                "md59149b9f2-7613-4f1b-9a92-5fad5e18eac4.MainActivity");
        desiredCapabilities.setCapability("autoDimissAlerts", true);

        //driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
        // driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Factory.createAndroidDriver(url, desiredCapabilities);
        URL url = new URL("http://localhost:4723/wd/hub");
        driver = Factory.createAndroidDriver(url, desiredCapabilities);
    }

    @Test
    public void canStartAppInTest() throws MalformedURLException, InterruptedException {

        //MobileElement elem = driver.findElementByName("com.moonpi.swiftnotes:id/newNote"));
       // elem.click();
        //Thread.sleep(5000);
        assertTrue(true);
    }

    @After
    public void TearDown(){
        driver.label("Stopping App");
        driver.quit();
    }
}
